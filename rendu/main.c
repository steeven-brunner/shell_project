/*
** main.c for  in /home/korovi_o/rendu/42sh/rendu
**
** Made by Olesya Korovina
** Login   <korovi_o@epitech.net>
**
** Started on  Fri Apr 25 13:21:42 2014 Olesya Korovina
** Last update Sun May 25 23:37:24 2014 Olesya Korovina
*/

#include <stdlib.h>
#include <stdio.h>
#include "env.h"
#include "lib.h"
#include "mysh.h"
#include "epur_str.h"

static void	show(t_node *node)
{
  t_node	*tmp;

  tmp = node;
  while (tmp != NULL)
    {
      printf("token : [%s]\n", tmp->token);
      printf("prio : [%d]\n", tmp->priority);
      tmp = tmp->next;
    }
}

int		main(int ac __attribute__ ((unused)),
		     char **av __attribute__ ((unused)), char **env)
{
  t_mysh	mysh;
  t_node	*node;
  char		*src;
  t_epur	*list;

  if ((mysh.myenv = env_to_list(env)) == NULL)
    {
      printf("No env\n");
      return (0);
    }
  mysh.gram = init_gram();
  mysh.fd_in = -1;
  mysh.fd_out = -1;
  while (42)
    {
      show_prompt(mysh.myenv);
      src = getnextline(0);
      if ((src = epur_str(src, list)) == NULL)
	return (127);
      node = parser(src, mysh.gram);
      show(node);
    }

  return (0);
}
