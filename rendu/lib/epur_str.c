/*
** cut_space.c for cut_space.c in /home/brunne_s/rendu/Communaute_du_C/minishell1/Cut_space
** 
** Made by brunner steeven
** Login   <brunne_s@epitech.net>
** 
** Started on  Thu Feb 13 15:33:40 2014 brunner steeven
** Last update Sun May 25 23:19:06 2014 Olesya Korovina
*/

#include <stdlib.h>
#include <stdio.h>
#include "epur_str.h"

static int	init_epur(t_epur **list)
{
  if ((*list = malloc(sizeof(t_epur))) == NULL)
    return (1);
  (*list)->i = 0;
  (*list)->mv_final = 0;
  return (0);
}

static void	add_space(char *str, t_epur *list)
{
  if (str[list->i] != '\0')
    {
      list->str_final[list->mv_final] = ' ';
      list->mv_final++;
    }
}

char	*epur_str(char *str, t_epur *list)
{
  if ((init_epur(&list)) == 1)
    return (NULL);
  while (str[list->i] == ' ' && str[list->i] != '\0')
    list->i++;
  list->str_final = malloc(my_strlen(str) * sizeof(*list->str_final));
  while (str[list->i] != '\0')
    {
      while (str[list->i] != ' ' && str[list->i] != '\0')
        {
          list->str_final[list->mv_final] = str[list->i];
          list->i++;
	  list->mv_final++;
        }
      while (str[list->i] == ' ' || str[list->i] == '\0')
        {
          if (str[list->i] == '\0')
	    {
	      list->str_final[list->mv_final] = '\0';
	      return (list->str_final);
	    }
	  list->i++;
	}
      add_space(str, list);
    }
}
