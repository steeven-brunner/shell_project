/*
** xmalloc.c for  in /home/panda/rendu/PSU_2013_minishell2
** 
** Made by Olesya Korovina
** Login   <korovi_o@epitech.net>
** 
** Started on  Thu Feb 27 11:19:21 2014 Olesya Korovina
** Last update Sun May 25 23:28:11 2014 Olesya Korovina
*/

#include <stdlib.h>
#include <unistd.h>
#include "env.h"
#include "lib.h"

void	*xmalloc(size_t size)
{
  void	*data;

  if ((data = malloc(size)) == NULL)
    {
      write(2, "Malloc : error\n", my_strlen("Malloc : error\n"));
      return (NULL);
    }
  return (data);
}
