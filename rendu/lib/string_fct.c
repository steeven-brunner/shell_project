/*
** string_fct.c for  in /home/panda/rendu/42sh/rendu
** 
** Made by Olesya Korovina
** Login   <korovi_o@epitech.net>
** 
** Started on  Sun May 25 23:24:49 2014 Olesya Korovina
** Last update Sun May 25 23:29:42 2014 Olesya Korovina
*/

#include "env.h"
#include "lib.h"

void	my_putchar(char c)
{
 write(1, &c, 1);
}

void	my_putstr(char *str)
{
  write(1, str, my_strlen(str));
}

void     my_put_nbr(int nb)
{
  if (nb < 0)
    {
      nb = nb * -1;
      my_putchar('-');
    }
  if (nb >= 10)
    {
      my_put_nbr(nb / 10);
      my_putchar(nb % 10 + '0');
    }
  if (nb >= 0 && nb <= 9)
    {
      my_putchar(nb + '0');
    }
}
