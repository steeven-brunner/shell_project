/* 
** grammar.c for  in /home/panda/rendu/PSU_2013_minishell2
** 
** Made by Olesya Korovina
** Login   <korovi_o@epitech.net>
** 
** Started on  Thu Feb 27 10:58:33 2014 Olesya Korovina
** Last update Fri May 23 19:14:50 2014 Olesya Korovina
*/

#include <stdlib.h>
#include "parsing.h"
#include "env.h"
#include "lib.h"

static t_gram		*add_node_gram(t_gram *node, char *src, int prio)
{
  t_gram        	*tmp;
  t_gram        	*new_node;

  tmp = node;
  new_node = xmalloc(sizeof(t_node));
  new_node->token = src;
  new_node->priority = prio;
  new_node->next = NULL;
  if (tmp == NULL)
    return (new_node);
  while (tmp->next != NULL)
    tmp = tmp->next;
  tmp->next = new_node;
  return (node);
}

t_gram		*init_gram()
{
  t_gram	*gram;

  gram = NULL;
  gram = add_node_gram(gram, my_strdup(";"), 7);
  gram = add_node_gram(gram, my_strdup("||"), 6);
  gram = add_node_gram(gram, my_strdup("&&"), 6);
  gram = add_node_gram(gram, my_strdup("|"), 5);
  gram = add_node_gram(gram, my_strdup("<<"), 4);
  gram = add_node_gram(gram, my_strdup(">>"), 2);
  gram = add_node_gram(gram, my_strdup("<"), 3);
  gram = add_node_gram(gram, my_strdup(">"), 1);
  return (gram);
}

t_gram		*is_grammar(char *str, int i, t_gram *gram)
{
  t_gram	*tmp;

  tmp = gram;
  while (tmp != NULL)
    {
      if (my_strncmp((str + i), tmp->token, my_strlen(tmp->token)) == 0)
	     return (tmp);
      tmp = tmp->next;
    }
  return (NULL);
}
