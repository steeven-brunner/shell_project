/*
** struct_epur_str.h for epur_str.h in /home/brunne_s/rendu/epur_str
** 
** Made by brunner steeven
** Login   <brunne_s@epitech.net>
** 
** Started on  Thu Feb 13 19:52:56 2014 brunner steeven
** Last update Sun May 25 23:13:33 2014 Olesya Korovina
*/

#ifndef STRUCT_EPUR_STR_
# define STRUCT_EPUR_STR_

typedef struct	s_epur
{
  int		i;
  int		mv_final;
  char		*str_final;
}		t_epur;

char	*epur_str(char *str, t_epur *list);

#endif /* !STRUCT_GET_PATH_ */
