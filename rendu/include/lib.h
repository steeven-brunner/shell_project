/*
** lib.h for  in /home/panda/rendu/42sh
** 
** Made by Olesya Korovina
** Login   <korovi_o@epitech.net>
** 
** Started on  Thu Feb 27 10:58:33 2014 Olesya Korovina
** Last update Sun May 25 23:27:17 2014 Olesya Korovina
*/

#ifndef LIB_H_
# define LIB_H_

int	my_strlen(char *);
void	*xmalloc(size_t);
int	my_strcmp(char *, char *);
char	*my_strdup(char *);
int	my_strncmp(char *, char *, int);
char	*getnextline(int);
void	xclose(int);
void	show_prompt(t_env *);
void	my_putchar(char);
void	my_putstr(char *);
void	my_put_nbr(int);

#endif /* !LIB_H_ */
