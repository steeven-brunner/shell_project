/*
** parsing.h for  in /home/panda/rendu/42sh/rendu/include
** 
** Made by Olesya Korovina
** Login   <korovi_o@epitech.net>
** 
** Started on  Mon May 12 14:20:26 2014 Olesya Korovina
** Last update Sat May 17 20:50:00 2014 Olesya Korovina
*/

#ifndef PARSING_H_
# define PARSING_H_

typedef struct	s_node
{
  char		*token;
  int		priority;
  struct s_node	*next;
}		t_node;

typedef struct	s_node t_gram;

typedef struct	s_tree
{
  char		*cmd;
  struct s_tree	*left;
  struct s_tree	*right;
}		t_tree;

/*
** FUNCTIONS
*/

t_gram		*init_gram();
t_gram		*is_grammar(char *, int, t_gram *);
t_node		*parser(char *, t_gram *);
int		get_prio(char *, int, t_gram *);

#endif /* !PARSING_H_ */
