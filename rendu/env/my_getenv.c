/*
** my_getenv.c for  in /home/panda/rendu/PSU_2013_minishell2
** 
** Made by Olesya Korovina
** Login   <korovi_o@epitech.net>
** 
** Started on  Tue Mar  4 09:23:00 2014 Olesya Korovina
** Last update Fri May  9 16:34:10 2014 Sattler Quentin
*/

#include <stdlib.h>
#include "env.h"
#include "lib.h"

char	*my_getenv(char *name, t_env *my_env)
{
  t_env	*tmp;

  tmp = my_env;
  while (tmp)
    {
      if (my_strcmp(name, tmp->name) == 0)
	return (tmp->data);
      tmp = tmp->next;
    }
  return (NULL);
}
