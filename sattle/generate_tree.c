/*
** generate_tree.c for 42 in /home/sattle_q/modules/sys_unix/42sh/my/parsing
** 
** Made by Sattler Quentin
** Login   <sattle_q@epitech.net>
** 
** Started on  Tue May 13 14:48:25 2014 Sattler Quentin
** Last update Sat May 24 11:51:29 2014 Sattler Quentin
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "parsing.h"
#include "lib.h"

static t_tree	*creat_tree_node(t_node	*pos)
{
  t_tree	*node;

  if ((node = malloc(sizeof(*node))) == NULL)
    return (NULL);
  if ((node->cmd = my_strdup(pos->token)) == NULL)
    return (NULL);
  node->left = NULL;
  node->right = NULL;
  return (node);
}

static void	move(t_node **tmp, t_node **prev2, int prio)
{
  t_node	*prev;
  t_node	*tmp2;

  prev = NULL;
  tmp2 = *tmp;
  while (*tmp)
    {
      if ((*tmp)->priority == prio)
	{
	  *prev2 = prev;
	  tmp2 = *tmp;
	}
      prev = *tmp;
      *tmp = (*tmp)->next;
    }
  *tmp = tmp2;
}

int		size(t_node *node)
{
  int		i;
  t_node	*tmp;

  tmp = node;
  i = 0;
  while (tmp)
    {
      i += 1;
      tmp = tmp->next;
    }
  return (i);
}

static void	rec_call(int prio, t_tree **start, t_node *node, t_node *next)
{
  if (prio == 5)
    {
      (*start)->left = construct_tree(next, prio);
      (*start)->right = construct_tree(node, prio);
    }
  else
    {
      (*start)->left = construct_tree(node, prio);
      (*start)->right = construct_tree(next, prio);
    }
}

t_tree		*construct_tree(t_node *node, int prio)
{
  t_tree	*start;
  t_node	*prev;
  t_node	*tmp;

  tmp = node;
  modif_prev(&prev, &tmp, 3, &start);
  while (tmp)
    if (tmp->priority == prio)
      {
	if ((prio == 6 || prio == 5) && size(node) != 3)
	  move(&tmp, &prev, prio);
	if ((start = creat_tree_node(tmp)) == NULL)
	  return (start);
	if (size(node) == 1)
	  {
	    free(tmp);
	    return (start);
	  }
	modif_prev(&prev, &tmp, 0, &start);
	rec_call(prio, &start, node, tmp->next);
	return (start);
      }
    else
      modif_prev(&prev, &tmp, 1, &start);
  construct_tree(node, prio - 1);
}
