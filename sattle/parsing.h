/*
** parsing.h for 42sh in /home/sattle_q/modules/sys_unix/42sh/42sh/sattle
** 
** Made by Sattler Quentin
** Login   <sattle_q@epitech.net>
** 
** Started on  Wed Apr  9 10:34:16 2014 Sattler Quentin
** Last update Wed Apr  9 16:52:28 2014 Sattler Quentin
*/

#ifndef PARSING_H_
# define PARSING_H_

typedef struct	s_node
{
  char		*token;
  int		priority;
  struct s_tree	*next;
}		t_node;

typedef struct	s_node t_gram;

typedef struct	s_tree
{
  char		*cmd;
  int		type;
  struct s_tree	*left;
  struct s_tree	*right;
}		t_tree;

/*
** FUNCTIONS
*/

t_gram		*init_gram();

#endif /* !PARSING_H_ */
