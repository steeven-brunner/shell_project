/*
** exec_pipe.c for 42 in /home/sattle_q/modules/sys_unix/42sh/my/pipe
** 
** Made by Sattler Quentin
** Login   <sattle_q@epitech.net>
** 
** Started on  Sat May 24 18:11:35 2014 Sattler Quentin
** Last update Sun May 25 17:49:06 2014 Sattler Quentin
*/

#include <unistd.h>
#include <stdlib.h>
#include "exec.h"

int		put_error(char *str)
{
  write(2, str, strlen(str));
  return (1);
}

static int	exec_cmd1(int *(pfd[]), char **cmd1, cyhar **cmd2,
			  t_mysh *mysh)
{
  int		fdf;
  int		status;

  if ((fdf = fork()) == -1)
    return (put_error("Can't perform fork()\n"));
  else if (fdf == 0)
    {
      close((*pfd)[0]);
      dup2(1, (*pfd)[1]);
      execve(cmd1[0], cmd1, create_env_tab(mysh->myenv));
      close((*pfd)[1]);
    }
  else
    {
      dup2((*pfd)[0], 0);
      execve(cmd2[0], cmd1, create_env_tab(mysh->myenv));
      if (wait(&status) == -1)
	return (1);
    }
  return (0);
}

int		exec_pipe(char *cmd1, char *cmd2, t_mysh *mysh)
{
  char		**arg_cmd1;
  char		**arg_cmd2;
  int		pfd[2];
  int		fdf;
  int		status;

  arg_cmd1 = my_str_to_wordtab(cmd1);
  arg_cmd2 = my_str_to_wordtab(cmd2);
  if (pipe(pfd) == -1)
    return (put_error("Can't perform pipe()\n"));
  if ((fdf = fork()) == -1)
    return (put_error("Can't perform fork()\n"));
  else if (fdf == 0)
    {
      if (exec_cmd(&pfd, arg_cmd1, arg_cmd2, mysh) == 1)
	exit (1);
    }
  else
    if (wait(&status) == -1)
      return (1);
  return (0);
}
