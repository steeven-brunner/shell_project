/*
** generate_tree_bis.c for 42 in /home/sattle_q/modules/sys_unix/42sh/my/parsing
** 
** Made by Sattler Quentin
** Login   <sattle_q@epitech.net>
** 
** Started on  Sat May 24 11:28:57 2014 Sattler Quentin
** Last update Sat May 24 11:51:55 2014 Sattler Quentin
*/

#include <stdlib.h>
#include "parsing.h"

void		modif_prev(t_node **prev, t_node **tmp, int op, t_tree **start)
{
  if (op == 3)
    {
      *tmp = *tmp;
      *prev = NULL;
      *start = NULL;
    }
  else if (op == 1)
    {
      *prev = *tmp;
      *tmp = (*tmp)->next;
      *start = *start;
    }
  else if (op == 0)
    {
      *tmp = *tmp;
      *start = *start;
      if (*prev)
	(*prev)->next = NULL;
    }
}
