/*
** echo.c for echo.c in /home/brunne_s/rendu/42sh/42sh/op_42
** 
** Made by brunner steeven
** Login   <brunne_s@epitech.net>
** 
** Started on  Sat May 24 17:53:04 2014 brunner steeven
** Last update Sat May 24 18:55:12 2014 brunner steeven
*/

#include "env.h"
#include "stdio.h"

void	option_first(char *str, t_env *env)
{
  t_env	*tmp;
  char	*str2;
  int	i;


  tmp = env;
  i = 1;
  str2 = malloc(my_strlen(str) * sizeof(*str2));
  while (str[i] != '\0')
    {
      str2[i - 1] = str[i];
      i++;
    }
  while (tmp != NULL)
    {
      if (strcmp(str2, tmp->name) == 0)
	{
	  printf("%s\n", tmp->data);
	  return ;
	}
      tmp = tmp->next;
    }
  printf("\n");
}

void	option_e()
{



}

void	third_option()
{



}

void	echo(char **tab, t_env *env, char *last_command, int returned_value)
{
  int	j;
  int	i;

  j = 1;
  i = 0;
  while (tab[j] != NULL)
    {
      if (tab[j][0] == '$' && tab[j][1] != '?') 
	option_first(tab[], env);
      else if (tab[j][0] == '$' && tab[j][1] == '?')
	printf("%d\n", returned_value);
      else if (tab[j][0] == '!' && tab[j][1] == '$')
	printf("echo %s\n%s\n", last_command, last_command);
      else if (tab[j][0] == '-' && tab[j][1] == 'e')
	option_e(tab);
    }
}
