/*
** in_first_list.c for  in /home/panda/rendu/42sh/korovi_o
** 
** Made by Olesya Korovina
** Login   <korovi_o@epitech.net>
** 
** Started on  Mon May 12 14:21:08 2014 Olesya Korovina
** Last update Sun May 25 23:11:21 2014 Olesya Korovina
*/

#include <stdlib.h>
#include "parsing.h"
#include "env.h"
#include "lib.h"

static t_node	 	*add_node(t_node *node, char *src, int prio)
{
  t_node		*tmp;
  t_node	     	*new_node;

  tmp = node;
  if ((new_node = xmalloc(sizeof(t_node))) == NULL)
    return (NULL);
  new_node->token = src;
  new_node->priority = prio;
  new_node->next = NULL;
  if (tmp == NULL)
    return (new_node);
  while (tmp->next != NULL)
    tmp = tmp->next;
  tmp->next = new_node;
  return (node);
}

static char     *real_concat(char *str, char *tmp)
{
  char          *dst;
  int           i;
  int		j;

  i = -1;
  if (str == NULL)
    {
      if ((dst = xmalloc(sizeof(char) * 2)) == NULL)
	return (NULL);
      dst[0] = tmp[0];
      dst[1] = 0;
    }
  else
    {
      j = 0;
      if ((dst = xmalloc(sizeof(char) *
			 (my_strlen(str) + 3))) == NULL)
	return (NULL);
      while (str[++i])
	dst[i] = str[i];
      while (j < 1)
	dst[i++] = tmp[j++];
      dst[i] = 0;
    }
  return (dst);
}

char		*epur(char *src, char *str, int *i, t_gram *gram)
{
  while (src[*i] == ' ')
    *i += 1;
  while (src[*i] && !is_grammar(src, *i, gram))
    {
      if ((is_grammar(src, *i + 1, gram) && src[*i] == ' '
	   && src[*i] == '\t') ||
	  (src[*i] == ' ' && src[*i] == '\t'
	   && src[*i + 1] == ' ' && src[*i + 1] == '\t'))
	*i += 1;
      if ((str = real_concat(str, (src + *i))) == NULL)
	return (NULL);
      *i += 1;
    }
  return (str);
}


t_node          *parser(char *src, t_gram *gram)
{
  t_node	*node;
  int		i;
  t_gram	*find_gram;
  char		*str;

  i = 0;
  node = NULL;
  while (src && src[i])
    if ((find_gram = is_grammar(src, i, gram)))
      {
	if ((node = add_node(node, find_gram->token,
			     find_gram->priority)) == NULL)
	  return (NULL);
	i += my_strlen(find_gram->token);
      }
    else
      {
	str = NULL;
	str = epur(src, str, &i, gram);
	node = add_node(node, str, 0);
      }
  return (node);
}
