/*
** generate_tree.c for 42 in /home/sattle_q/modules/sys_unix/42sh/my/parsing
** 
** Made by Sattler Quentin
** Login   <sattle_q@epitech.net>
** 
** Started on  Tue May 13 14:48:25 2014 Sattler Quentin
** Last update Fri May 23 18:51:16 2014 brunner steeven
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "parsing.h"
#include "lib.h"

static t_tree	*creat_tree_node(t_node	*pos)
{
  t_tree	*node;

  if ((node = malloc(sizeof(*node))) == NULL)
    return (NULL);
  if ((node->cmd = my_strdup(pos->token)) == NULL)
    return (NULL);
  node->left = NULL;
  node->right = NULL;
  return (node);
}

static void	move(t_node **tmp, t_node **prev2, int prio)
{
  t_node	*prev;
  t_node	*tmp2;

  prev = NULL;
  tmp2 = *tmp;
  while (*tmp)
    {
      if ((*tmp)->priority == prio)
	{
	  *prev2 = prev;
	  tmp2 = *tmp;
	}
      prev = *tmp;
      *tmp = (*tmp)->next;
    }
  /* if (strcmp(prev->token, "|") == 0 || (*tmp)->next == NULL) */
  /*   { */
  /*     prev->next = NULL; */
  /*     free(*tmp); */
  /*     *tmp = prev; */
  /*   } */
  /* else */
    *tmp = tmp2;
}

static int	size(t_node *node)
{
  int		i;
  t_node	*tmp;

  tmp = node;
  i = 0;
  while (tmp)
    {
      i += 1;
      tmp = tmp->next;
    }
  return (i);
}

t_tree		*construct_tree(t_node *node, int prio)
{
  t_tree	*start;
  t_node	*prev;
  t_node	*tmp;

  tmp = node;
  prev = NULL;
  start = NULL;
  if (tmp == NULL)
    return (start);
  while (tmp)
    {
      if (tmp->priority == prio)
	{
	  /*sleep(1);*/
	  printf("token = [%s]\tnode = [%s]\n", tmp->token, node->token);
	  if (prio == 6 || prio == 5)
	    move(&tmp, &prev, prio);
	  if ((start = creat_tree_node(tmp)) == NULL)
	    return (start);
	  if (size(node) == 1)
	    {
	      free(tmp);
	      return (start);
	    }
	  if (prev)
	    prev->next = NULL;
	  if (prio == 5)
	    {
	      start->left = construct_tree(tmp->next, prio);
	      start->right = construct_tree(node, prio);
	    }
	  else
	    {
	      start->left = construct_tree(node, prio);
	      start->right = construct_tree(tmp->next, prio);
	    }
	  return (start);
	}
      else
	{
	  prev = tmp;
	  tmp = tmp->next;
	}
      construct_tree(node, prio - 1);
    }
}
