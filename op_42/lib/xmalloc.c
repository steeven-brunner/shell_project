/*
** xmalloc.c for  in /home/panda/rendu/PSU_2013_minishell2
** 
** Made by Olesya Korovina
** Login   <korovi_o@epitech.net>
** 
** Started on  Thu Feb 27 11:19:21 2014 Olesya Korovina
** Last update Sat May 17 17:54:27 2014 Olesya Korovina
*/

#include <stdlib.h>
#include <unistd.h>
#include "lib.h"

void	*xmalloc(size_t size)
{
  void	*data;

  if ((data = malloc(size)) == NULL)
    {
      write(2, "Malloc : error\n", my_strlen("Malloc : error\n"));
      exit(EXIT_FAILURE);
    }
  return (data);
}
