/*
** xclose.c for  in /home/panda/rendu/PSU_2013_minishell2
** 
** Made by Olesya Korovina
** Login   <korovi_o@epitech.net>
** 
** Started on  Thu Feb 27 12:06:32 2014 Olesya Korovina
** Last update Mon Apr 14 14:27:38 2014 Olesya Korovina
*/

#include <stdlib.h>
#include <unistd.h>
#include "lib.h"

void	xclose(int fd)
{
  if (close(fd) == -1)
    {
      write(2, "Close : error\n", my_strlen("Close : error\n"));
      exit(EXIT_FAILURE);
    }
}
