/*
** my_putchar.c for putchar in /home/brunne_s/rendu/Piscine-C-lib/my
** 
** Made by brunner steeven
** Login   <brunne_s@epitech.net>
** 
** Started on  Mon Oct 21 10:51:08 2013 brunner steeven
** Last update Mon Oct 21 10:51:39 2013 brunner steeven
*/

void	my_putchar(char c)
{
  write(1, &c, 1);
}
