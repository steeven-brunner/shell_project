/*
** getnextline.c for  in /home/panda
** 
** Made by Olesya Korovina
** Login   <korovi_o@epitech.net>
** 
** Started on  Fri Feb 28 21:38:41 2014 Olesya Korovina
** Last update Mon May 12 13:50:11 2014 Olesya Korovina
*/

#include <stdlib.h>
#include <unistd.h>
#include "lib.h"
#include "getnextline.h"

static int	seek_enter(char *str)
{
  int		i;

  i = 0;
  while (str[i])
    {
      if (str[i] == '\n')
	return (1);
      i++;
    }
  return (0);
}

static char    	*realloc_and_concatains(char *str, char tmp[SIZE_READ + 1])
{
  char		*dst;
  int		i;
  int		j;

  i = -1;
  if (str == NULL)
    {
      dst = xmalloc(sizeof(char) + (my_strlen(tmp) + 1));
      while (tmp[++i])
	dst[i] = tmp[i];
      dst[i] = 0;
    }
  else
    {
      j = 0;
      dst = xmalloc(sizeof(char) * (my_strlen(tmp) + my_strlen(str) + 1));
      while (str[++i])
	dst[i] = str[i];
      free(str);
      while (tmp[j])
	dst[i++] = tmp[j++];
      dst[i] = 0;
    }
  return (dst);
}

static char	*read_line(int fd, char *str)
{
  char		tmp[SIZE_READ + 1];
  int		size_count;

  size_count = read(0, &tmp, SIZE_READ);
  if (size_count <= 0)
    return (str);
  tmp[size_count] = 0;
  str = realloc_and_concatains(str, tmp);
  if (str == NULL)
    return (NULL);
  if (seek_enter(tmp))
    return (str);
  return (read_line(fd, str));
}

char			*getnextline(int fd)
{
  static char		*str = NULL;
  static unsigned int	i = 0;
  char			*dst;
  int			j;

  j = i;
  if (str == NULL || !seek_enter(str + i))
    if ((str = read_line(fd, str)) == NULL || str[i] == 0)
      return (NULL);
  while (str[j] && str[j] != '\n')
    j++;
  dst = xmalloc(sizeof(char) * (j - i + 1));
  j = 0;
  while (str[i] && str[i] != '\n')
    {
      dst[j] = str[i];
      j++;
      i++;
    }
  dst[j] = 0;
  if (str[i] == '\n')
    i++;
  return (dst);
}
