/*
** my_putstr.c for my_putstr in /home/brunne_s/rendu/Piscine-C-Jour_04
** 
** Made by brunner steeven
** Login   <brunne_s@epitech.net>
** 
** Started on  Thu Oct  3 11:28:55 2013 brunner steeven
** Last update Sun Oct 27 16:01:41 2013 brunner steeven
*/

int	my_putstr(char *str)
{
  int	c;

  c = 0;
  while (str[c] != '\0')
    {
      my_putchar(str[c]);
      c = c + 1;
    }
}
