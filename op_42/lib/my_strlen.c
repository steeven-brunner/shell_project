/*
** my_strlen.c for my_strlen.c in /home/brunne_s/rendu/42sh/op_42/lib
** 
** Made by brunner steeven
** Login   <brunne_s@epitech.net>
** 
** Started on  Fri May 23 19:46:20 2014 brunner steeven
** Last update Fri May 23 19:46:49 2014 brunner steeven
*/

int	my_strlen(char *str)
{
  int	i;

  i = 0;
  while (str[i] != '\0')
    i++;
  return (i);
}
