/*
** cut_space.c for cut_space.c in /home/brunne_s/rendu/Communaute_du_C/minishell1/Cut_space
** 
** Made by brunner steeven
** Login   <brunne_s@epitech.net>
** 
** Started on  Thu Feb 13 15:33:40 2014 brunner steeven
** Last update Sun May 25 23:45:48 2014 Olesya Korovina
*/

#include <stdlib.h>
#include <stdio.h>
#include "epur_str.h"
#include "env.h"
#include "lib.h"

static int	init_epur(t_epur **list)
{
  if ((*list = malloc(sizeof(t_epur))) == NULL)
    return (1);
  (*list)->i = 0;
  (*list)->mv_final = 0;
  return (0);
}

static void	add_space(char *str, t_epur *list)
{
  if (str[list->i] != '\0')
    {
      list->str_f[list->mv_final] = ' ';
      list->mv_final++;
    }
}

char	*epur_str(char *str, t_epur *l)
{
  if ((init_epur(&l)) == 1)
    return (NULL);
  while (str[l->i] == ' ' && str[l->i] != '\0')
    l->i++;
  if ((l->str_f = xmalloc(my_strlen(str) * sizeof(*l->str_f))) == NULL)
      return (NULL);
  while (str[l->i] != '\0')
    {
      while (str[l->i] != ' ' && str[l->i] != '\0')
        {
          l->str_f[l->mv_final] = str[l->i];
          l->i++;
	  l->mv_final++;
        }
      while (str[l->i] == ' ' || str[l->i] == '\0')
        {
          if (str[l->i] == '\0')
	    {
	      l->str_f[l->mv_final] = '\0';
	      return (l->str_f);
	    }
	  l->i++;
	}
      add_space(str, l);
    }
}
