/*
** lib.c for  in /home/panda/rendu/PSU_2013_minishell2
** 
** Made by Olesya Korovina
** Login   <korovi_o@epitech.net>
** 
** Started on  Thu Feb 27 11:23:04 2014 Olesya Korovina
** Last update Sat May 17 21:09:16 2014 Olesya Korovina
*/

#include "lib.h"

int	my_strlen(char *str)
{
  int	i;

  i = 0;
  while (str[i])
    i++;
  return (i);
}

int	my_strcmp(char *s1, char *s2)
{
  int	i;

  i = 0;
  while (s1[i] || s2[i])
    {
      if (s1[i] > s2[i])
	return (1);
      else if (s1[i] < s2[i])
	return (-1);
      i++;
    }
  return (0);
}

char	*my_strdup(char *str)
{
  char	*dest;
  int	i;

  i = 0;
  dest = xmalloc(sizeof(char) * (my_strlen(str) + 1));
  while (str[i])
    {
      dest[i] = str[i];
      i++;
    }
  dest[i] = '\0';
  return (dest);
}

int	my_strncmp(char *s1, char *s2, int n)
{
  int	i;

  i = 0;
  while (i < n)
    {
      if (s1[i] != s2[i])
	return (s1[i] - s2[i]);
      if (!s1[i])
	return (0);
      i++;
    }
  return (0);
}
