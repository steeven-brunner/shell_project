/*
** my_put_nbr.c for my_put_nbr.c in /home/brunne_s/rendu/Piscine-C-Jour_03
** 
** Made by brunner steeven
** Login   <brunne_s@epitech.net>
** 
** Started on  Thu Oct  3 17:44:58 2013 brunner steeven
** Last update Mon Jan 20 17:09:14 2014 brunner steeven
*/

int	my_put_nbr(int nb)
{
  if (nb < 0)
    {
      nb = nb * -1;
      my_putchar('-');
    }
  if (nb >= 10)
    {
      my_put_nbr(nb / 10);
      my_putchar(nb % 10 + '0');
    }
 if (nb >= 0 && nb <= 9)
    {
      my_putchar(nb + '0');
    }
  return (0);
}

