/*
** free_exit.c for  in /home/korovi_o/rendu/42sh/korovi_o
** 
** Made by Olesya Korovina
** Login   <korovi_o@epitech.net>
** 
** Started on  Mon Apr 14 14:24:46 2014 Olesya Korovina
** Last update Mon Apr 14 14:28:00 2014 Olesya Korovina
*/

#include <stdlib.h>
#include "env.h"
#include "lib.h"

void		free_exit(t_env *env)
{
  t_env		*tmp;
  t_env		*save;

  if (env != NULL)
    {
      tmp = env;
      while (tmp)
	{
	  save = tmp->next;
	  free(tmp->data);
	  free(tmp->name);
	  free(tmp);
	  tmp = save;
	}
    }
  exit(0);
}
