/*
** search_env.c for  in /home/panda/rendu/PSU_2013_minishell2
** 
** Made by Olesya Korovina
** Login   <korovi_o@epitech.net>
** 
** Started on  Tue Mar  4 09:30:40 2014 Olesya Korovina
** Last update Mon Apr 14 13:56:46 2014 Olesya Korovina
*/

#include <stdlib.h>
#include "env.h"
#include "lib.h"

t_env	*search_env(t_env *env, char *name)
{
  t_env	*tmp;

  tmp = env;
  while (tmp)
    {
      if (my_strcmp(name, tmp->name) == 0)
	return (tmp);
      tmp = tmp->next;
    }
  return (NULL);
}

t_env	*old_pwd(t_env *env)
{
  t_env	*tmp;
  t_env	*new_node;

  if ((my_getenv("OLDPWD", env) == NULL) && (my_getenv("PWD", env) != NULL))
    {
      new_node = xmalloc(sizeof(t_env));
      new_node->name = my_strdup("OLDPWD");
      new_node->data = my_strdup(my_getenv("PWD", env));
      new_node->next = NULL;
      if (env == NULL)
	return (new_node);
      tmp = env;
      while (tmp->next)
	tmp = tmp->next;
      tmp->next = new_node;
      return (env);
    }
  return (env);
}