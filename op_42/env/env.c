/*
** env.c for  in /home/panda/rendu/PSU_2013_minishell2
** 
** Made by Olesya Korovina
** Login   <korovi_o@epitech.net>
** 
** Started on  Tue Mar  4 09:29:11 2014 Olesya Korovina
** Last update Sat May 17 17:52:07 2014 Olesya Korovina
*/

#include <stdlib.h>
#include "env.h"
#include "lib.h"

static int	env_len(char *str)
{
  int		i;

  i = 0;
  while (str[i] && str[i] != '=')
    i++;
  return (i);
}

static char	*name_env(char *name_env)
{
  int		i;
  char		*name;

  i = 0;
  name = xmalloc(sizeof(char) * (env_len(name_env) + 1));
  while (name_env[i] && name_env[i] != '=')
    {
      name[i] = name_env[i];
      i++;
    }
  name[i] = 0;
  return (name);
}

static char	*data_env(char *env)
{
  int		i;
  char		*data;
  int		j;

  i = 0;
  j = env_len(env) + 1;
  data = xmalloc(sizeof(char) * (my_strlen(env + j) + 1));
  while (env[i + j])
    {
      data[i] = env[i + j];
      i++;
    }
  data[i] = 0;
  return (data);
}

static t_env	*node_env(t_env *my_env, char *env)
{
  t_env		*new_node;
  t_env		*tmp;

  new_node = xmalloc(sizeof(t_env));
  new_node->name = name_env(env);
  new_node->data = data_env(env);
  new_node->next = NULL;
  if (my_env == NULL)
    return (new_node);
  tmp = my_env;
  while (tmp->next)
    tmp = tmp->next;
  tmp->next = new_node;
  return (my_env);
}

t_env	*env_to_list(char **env)
{
  int	i;
  t_env	*my_env;

  i = 0;
  my_env = NULL;
  while (env[i])
    {
      my_env = node_env(my_env, env[i]);
      i++;
    }
  return (old_pwd(my_env));
}
