/* 
** env.h for  in /home/panda/rendu/PSU_2013_minishell2
** 
** Made by Olesya Korovina
** Login   <korovi_o@epitech.net>
** 
** Started on  Thu Feb 27 10:58:33 2014 Olesya Korovina
** Last update Mon Apr 14 14:26:03 2014 Olesya Korovina
*/

#ifndef ENV_H_
# define ENV_H_

/*
** STRUCTURES
*/

typedef struct	s_env
{
  char		*name;
  char		*data;
  struct s_env	*next;
}		t_env;

/*
** ENV
*/

char	*my_getenv(char *, t_env *);
t_env	*search_env(t_env *, char *);
t_env	*old_pwd(t_env *);
t_env	*env_to_list(char **);
void	free_exit(t_env *);

# endif /* !ENV_H_ */
