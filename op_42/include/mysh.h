/* 
** mysh.h for  in /home/panda/rendu/PSU_2013_minishell2
** 
** Made by Olesya Korovina
** Login   <korovi_o@epitech.net>
** 
** Started on  Thu Feb 27 10:58:33 2014 Olesya Korovina
** Last update Thu May 22 21:13:36 2014 brunner steeven
*/

#ifndef MYSH_H_
# define MYSH_H_

# include "parsing.h"

typedef	struct	s_prev
{
  int		ret;
  char		*last;
}		t_prev;

typedef struct	s_mysh
{
  t_env		*myenv;
  t_gram	*gram;
  t_prev	*prev;
  int		fd_in;
  int		fd_out;
  int		*pid;
  int		fd_p[2];
  int		old_fd;
  int		nb_pipe;
}		t_mysh;

#endif /* !MYSH_H_ */
