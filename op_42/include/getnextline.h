/* 
** getnextline.h for  in /home/panda/rendu/PSU_2013_minishell2
** 
** Made by Olesya Korovina
** Login   <korovi_o@epitech.net>
** 
** Started on  Thu Feb 27 10:58:33 2014 Olesya Korovina
** Last update Mon Apr 14 14:26:03 2014 Olesya Korovina
*/

#ifndef GETNEXTLINE_H_
# define GETNEXTLINE_H_

# define SIZE_READ 255

#endif /* !GETNEXTLINE_H_ */