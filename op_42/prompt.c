/*
** prompt.c for  in /home/korovi_o/rendu/42sh/korovi_o
** 
** Made by Olesya Korovina
** Login   <korovi_o@epitech.net>
** 
** Started on  Mon Apr 14 13:57:57 2014 Olesya Korovina
** Last update Sun May 25 23:21:34 2014 Olesya Korovina
*/

#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <signal.h>
#include "env.h"
#include "lib.h"

static char	*show_folder(char *str)
{
  int		i;

  i = 0;
  while (str[i])
    i++;
  while (str[i] != '/' && i != 0)
    i--;
  if (str[i] == '/')
    return (str + i + 1);
  return (str + i);
}

void		show_prompt(t_env *env)
{
  static int	i = 1;

  if (my_getenv("USER", env) == NULL || my_getenv("PWD", env) == NULL)
    printf("$ %d > ", i);
  else
    {
      my_putchar('[');
      my_putstr(my_getenv("USER", env));
      my_putchar('@');
      my_putstr((show_folder(my_getenv("PWD", env))));
      my_putchar(' ');
      my_put_nbr(i);
      my_putstr("]$ ");
    }
  i++;
}

/*static void     close_fd(t_mysh *mysh)
{
  if (mysh->fd_in != -1)
    {
      xclose(mysh->fd_in);
      mysh->fd_in = -1;
    }
  if (mysh->fd_out != -1)
    {
      xclose(mysh->fd_out);
      mysh->fd_out = -1;
    }
}

void            prompt(t_mysh *mysh)
{
  char          buffer[255];
  int           ret;

  show_prompt(mysh->myenv);
  if ((ret = read(0, buffer, 254)) == -1)
    {
      printf("Error Read\n");
      free_exit(mysh->myenv);
    }
  if (buffer[0] == 0 || ret == 0 || ret == 1)
    return;
  if (buffer[ret - 1] == '\n')
    buffer[ret - 1] = '\0';
  else
    buffer[ret] = '\0';
  close_fd(mysh);
  return;
  }
*/
