/*
** parcours_tree.c for 42tmp in /home/sattle_q/modules/sys_unix/42sh/my/parsing
** 
** Made by Sattler Quentin
** Login   <sattle_q@epitech.net>
** 
** Started on  Wed May 14 15:19:42 2014 Sattler Quentin
** Last update Mon May 19 14:21:01 2014 Sattler Quentin
*/

#include <stdlib.h>
#include <stdio.h>
#include "parsing.h"

void		move_left(t_tree *node, int tab, int option)
{
  int		i;

  i = -1;
  if (node != NULL)
    {
      while (++i < tab)
	printf("\t");
      if (option == 1)
	printf("%s\n", node->cmd);
      else
	printf("%s", node->cmd);
      move_left(node->left, tab - 1, 0);
      move_left(node->right, tab - 1, 1);
    }
}

void		move_in_tree(t_tree *A)
{
  int		tab;
  int		i;

  i = -1;
  tab = 5;
  if (A != NULL)
    {
      while (++i < tab)
	printf("\t");
      printf("%s\n", A->cmd);
      move_left(A->left, tab - 1, 0);
      move_left(A->right, tab - 1, 1);
    }
}
