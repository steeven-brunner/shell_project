/*
** path_for_exec.c for path_for_exec.c in /home/brunne_s/rendu/42sh/42sh/op_42/exec
** 
** Made by brunner steeven
** Login   <brunne_s@epitech.net>
** 
** Started on  Sat May 24 16:50:40 2014 brunner steeven
** Last update Sun May 25 21:35:53 2014 brunner steeven
*/

#include <stdlib.h>
#include "exec.h"
#include "env.h"
#include "lib.h"

int	count_elem_path(t_env *env)
{
  t_env	*tmp;
  int	nb_elem;
  int	i;

  tmp = env;
  nb_elem = 0;
  i = 0;
  while (tmp != NULL)
    {
      if (strcmp(env->name, "PATH") == 0)
        {
          while (env->data[i] != '\0')
            {
              if (env->data[i] == ':')
                nb_elem++;
              i++;
            }
          return (nb_elem);
        }
      tmp = tmp->next;
    }
}

int	        find_position(char *str)
{
  static int	pos = 0;

  while (str[pos] != '\0')
    {
      if (str[pos] == ':')
	{
	  pos++;
	  return (pos);
	}
      if (str[pos] == '\0')
	pos = 0;
      pos++;
    }
}

int	find_size_malloc(int pos, char *str)
{
  printf("c = %c\n", str[pos]);
  printf("str = %s\n", str);
  printf("pos = %d\n", pos);
  while (str[pos] != '\0' && str[pos] != ':')
    {
      pos++;
      printf("YOLO\n");
    }
  printf("c2 = %c\n", str[pos]);
  return (pos);
}

char	**create_tab_path(t_env *env)
{
  t_env	*tmp;
  char	**tab_path;
  int	nb_elem;
  int	pos;
  int	size_malloc;
  int	i;

  tmp = env;
  pos = 0;
  i = 0;
  nb_elem = count_elem_path(env);
  tab_path = malloc(nb_elem * sizeof(*tab_path));
  while (tmp->name != NULL)
    {
      printf("LOL\n");
      if (strcmp(tmp->name, "PATH") == 0)
        {
	  printf("data = %s\n", tmp->data);
	  while (pos <= my_strlen(tmp->data))
	    {
	      pos = find_position(tmp->data);
	      size_malloc = find_size_malloc(pos, tmp->data);
	      tab_path[i] = malloc(size_malloc * sizeof(**tab_path));
	    }
	}
      tmp = tmp->next;
    }
}
