/*
** exec.c for exec.c in /home/brunne_s/rendu/42sh/op_42/exec
** 
** Made by brunner steeven
** Login   <brunne_s@epitech.net>
** 
** Started on  Sat May 24 11:14:27 2014 brunner steeven
** Last update Sun May 25 18:16:39 2014 brunner steeven
*/

#include <stdlib.h>
#include "env.h"
#include "exec.h"
#include "lib.h"

char	*create_str_search(char *command)
{
  int	i;
  char	*str_search;

  i = 0;
  while (command[i] != '\0' && command[i] != ' ')
    i++;
  str_search = malloc((i + 1)* sizeof(*str_search));
  i = 0;
  while (command[i] != '\0' && command[i] != ' ')
    {
      str_search[i] = command[i];
      i++;
    }
  return (str_search);
}

void	exec(char *command, t_env *env)
{
  char	**tab_search;
  char	**tab_env;
  char	**tab_path;
  int	mv_tab;

  tab_search = my_str_to_wordtab(command);
  tab_env = create_env_tab(env);
  tab_path = create_tab_path(env);
  mv_path = 0;
  if (fork == 0)
    {
      while (tab_path[mv_path] != NULL)
	{
	  if (execve(str_search, tab_search, tab_env) == - 1)
	    mv_tab++;
	  else
	    return ;
	}
      printf("[Error] : exec failed\n");
    }
  wait();
}
