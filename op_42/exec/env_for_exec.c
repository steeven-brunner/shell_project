/*
** env_for_exec.c for env_for_exec.c in /home/brunne_s/rendu/42sh/42sh/op_42/exec
** 
** Made by brunner steeven
** Login   <brunne_s@epitech.net>
** 
** Started on  Sat May 24 15:52:24 2014 brunner steeven
** Last update Sun May 25 18:16:16 2014 brunner steeven
*/

#include <stdlib.h>
#include "exec.h"
#include "env.h"
#include "lib.h"

int	count_elem_env(t_env *env)
{
  int	nb_elem;
  t_env	*tmp;

  nb_elem = 0;
  while (tmp != NULL)
    nb_elem++;
  return (nb_elem);
}

char	**create_env_tab(t_env *env)
{
  t_env	*tmp;
  char	**env_tab;
  int	nb_elem;
  int	i;

  tmp = env;
  nb_elem = count_elem_env(env);
  env_tab = malloc((nb_elem + 1) * sizeof(*env_tab));
  i = 0;
  while (tmp != NULL)
    {
      env_tab[i] = malloc((env->name + env->data + 1) * sizeof(**env_tab));
      strcpy(env_tab[i], env->name);
      strcpy(env_tab[i], "=");
      strcpy(env_tab[i], env->data);
      tmp = tmp->next;
      i++;
    }
  env_tab[i] = NULL;
  return (env_tab);
}
