/*
** exec.h for exec.h in /home/brunne_s/rendu/42sh/42sh/op_42/exec
** 
** Made by brunner steeven
** Login   <brunne_s@epitech.net>
** 
** Started on  Sat May 24 15:42:42 2014 brunner steeven
** Last update Sun May 25 18:53:13 2014 brunner steeven
*/

#ifndef EXEC_H_
# define EXEC_H_

#include "env.h"

char	*create_str_search(char *);
int	count_elem_env(t_env *);
char	**create_env_tab(t_env *);
char	**create_tab_path(t_env *);

#endif /* !EXEC_H_ */
