/*
** minishell1.c for minishell1 in /home/decamp_t/rendu/PSU_2013_minishell1
** 
** Made by decamp_t
** Login   <decamp_t@epitech.net>
** 
** Started on  Fri Dec 20 17:12:40 2013 decamp_t
** Last update Tue May 20 19:16:58 2014 decamp_t
*/

#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <signal.h>

#include "minishell.h"
#include "my_strcmp.h"
#include "minishell1.h"
#include "my_str_to_wordtab.h"

int	get_path(char **ep, int i)
{
  i = 0;
  while (my_strcmp(ep[i], "PATH") != 0)
    i = i + 1;
  return (i);
}

int	my_access(char **path)
{
  int	result;
  int	i;

  result = 0;
  i = 0;
  while (path[i] != 0)
    {
      result = access (path[i], F_OK);
      if (result == 0)
	return (i);
      i = i + 1;
    }
  if (result == -1)
    {
      write(1, "Commande introuvable\n", 21);
      write(1, "\n\r [1m=->[0m ", 15);
      exit (-1);
    }
}

char	command_2(char *buffer, char **ep)
{
  char	**tab;

  tab = my_str_to_wordtab(buffer);
  execve(tab[0], tab, ep);
  exit(0);
}

char	command(char *buffer, char **ep)
{
  char	**tab;
  char	**path;
  int	i;
  int	n;
  int	result;

  result = 0;
  n = 0;
  tab = my_str_to_wordtab(buffer);
  i = get_path(ep, i);
  path = my_path_to_wordtab(ep[i], tab[0]);
  n = my_access(path);
  i = 0;
  tab[0] = malloc((my_strlen(path[n]) * sizeof(char *)));
  while (path[i] && path[i] != 0)
    {
      tab[0][i] = path[n][i];
      i++;
    }
  tab[0][i] = 0;
  execve(path[n], tab, ep);
}

int	my_exec(char *buffer, pid_t shell, char **ep)
{
  if ((my_strcmp(buffer, "/bin/") == 0) ||
      (my_strcmp(buffer, "/usr/bin/") == 0))
    command_2(buffer, ep);
  else
    command(buffer, ep);
  exit (0);
}
