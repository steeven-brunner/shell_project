/*
** echo.c for dash in /home/decamp_t/rendu/42sh/decamp_t
** 
** Made by decamp_t
** Login   <decamp_t@epitech.net>
** 
** Started on  Tue May 13 15:33:53 2014 decamp_t
** Last update Sat May 24 22:46:26 2014 decamp_t
*/

#include <stdio.h>
#include "mysh.h"

int	my_echo_ret(char *str, t_ret prev)
{
  printf("%d\n", prev->ret);
  return (0);
}

int	my_echo_env(char *str, t_env *myenv)
{
  if (myenv != NULL)
    {
      while (myenv->next != NULL)
	{
	  if (strcmp(myenv->name, str) == 0)
	    {
	      printf("%s\n", myenv->data);
	      return (0);
	    }
	  myenv = ->next;
	}
    }
  printf("\n");
  return (1);
}

int	my_echo(char **str, t_mysh *mysh)
{
  int	i;

  i = 1;
  if (str[i] == NULL)
    printf("\n");
  else
    while (str[i] != NULL)
      {
	if (str[i][0] == '-' && str[i][1] == 'n')
	  {
	    mysh->prev->ret = no_back(str, mysh);
	    return (mysh->prev->ret);
	  }
	else if (str[i][0] == '$' && str[i][1] != '?')
	  mysh->prev->ret = my_echo_env((str[i] + 1), mysh->myenv);
	else if (str[i][0] == '$' && str[i][1] == '?')
	  mysh->prev->ret = my_echo_ret(str[i], mysh->prev);
	else
	  {
	    printf("%s\n", str[i]);
	    mysh->prev->ret = 0;
	  }
	i++;
      }
  return (mysh->prev->ret);
}
