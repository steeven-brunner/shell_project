/*
** my_strncmp.h for p in /home/decamp_t/rendu/PSU_2013_minishell1
** 
** Made by decamp_t
** Login   <decamp_t@epitech.net>
** 
** Started on  Fri Dec 20 16:41:53 2013 decamp_t
** Last update Mon Jan  6 15:00:27 2014 decamp_t
*/

#ifndef MY_STRNCMP_H_
# define MY_STRNCMP_H_

int     my_strcmp(char *s1, char *s2);

#endif /* !MY_STRNCMP_H_ */
