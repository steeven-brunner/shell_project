/*
** my_str_to_wordtab.c for word in /home/decamp_t/rendu/Piscine-C-lib/my
** 
** Made by decamp_t
** Login   <decamp_t@epitech.net>
** 
** Started on  Thu Dec 19 14:30:30 2013 decamp_t
** Last update Fri Jan  3 11:15:25 2014 decamp_t
*/

#include <stdlib.h>
#include "my_str_to_wordtab.h"

int	my_strlen(char *str)
{
  int	i;

  i = 0;
  while (str[i] != 0)
    i++;
  return (i);
}

int	calc_nbr_word(char *str)
{
  int	i;
  int	w;

  i = 0;
  w = 1;
  while (str[i] != 0)
    {
      if (str[i] == ' ' && str[i + 1] != ' ')
	w = w + 1;
      i = i + 1;
    }
  return (w);
}

int	calc_nbr_car(char *str, int *i)
{
  int	c;

  c = 0;
  while (str[*i] != ' ' && str[*i] != '\0' && str[*i] != '\n')
    {
      c = c + 1;
      *i = *i + 1;
    }
  return (c);
}

char	*my_strt(char *src)
{
  char	*ret;
  int	i;

  ret = malloc(my_strlen(src) * sizeof(char *));
  i = 0;
  while (src[i] != '\0')
    {
      ret[i] = src[i];
      i++;
    }
  return (ret);
}

char	**my_str_to_wordtab(char *str)
{
  int	i;
  int	m;
  int	l;
  char	**tab;

  tab = malloc(sizeof(char *) * calc_nbr_word(str));
  m = 0;
  i = 0;
  while (str[i] != 0)
    {
      if (str[i] != ' ')
	{
	  tab[m] = my_strt(str + i);
	  tab[m][calc_nbr_car(str, &i)] = '\0';
	  m = m + 1;
	}
      i = i + 1;
    }
  tab[calc_nbr_word(str)] = 0;
  return (tab);
}
