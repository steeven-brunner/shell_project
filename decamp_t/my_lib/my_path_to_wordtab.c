/*
** my_str_to_wordtab.c for word in /home/decamp_t/rendu/Piscine-C-lib/my
** 
** Made by decamp_t
** Login   <decamp_t@epitech.net>
** 
** Started on  Thu Dec 19 14:30:30 2013 decamp_t
** Last update Mon Jan  6 19:00:27 2014 decamp_t
*/

#include <stdlib.h>
#include "my_str_to_wordtab.h"

int	calc_nbr_word_p(char *str)
{
  int	i;
  int	w;

  i = 0;
  w = 1;
  while (str[i] != 0)
    {
      if (str[i] == ':' && str[i + 1] != ':')
	w = w + 1;
      i = i + 1;
    }
  return (w);
}

int	calc_nbr_car_p(char *str, int *i)
{
  int	c;

  c = 0;
  while (str[*i] != ':' && str[*i] != '\0' && str[*i] != '\n')
    {
      c = c + 1;
      *i = *i + 1;
    }
  return (c);
}

char	*my_patht(char *src, char *command)
{
  char	*ret;
  int	i;
  int	n;

  ret = malloc(((my_strlen(src) + my_strlen(command) + 1) * sizeof(char *)));
  i = 0;
  n = 0;
  while (src[i] && src[i] != ':')
    {
      ret[i] = src[i];
      i++;
    }
  ret[i] = '/';
  i = i + 1;
  while (command[n] != 0)
    {
      ret[i] = command[n];
      i++;
      n++;
    }
  ret[i] = '\0';
  return (ret);
}

char	**my_path_to_wordtab(char *str, char *command)
{
  int	i;
  int	m;
  int	l;
  char	**tab;

  tab = malloc(sizeof(char *) * calc_nbr_word_p(str));
  m = 0;
  i = 5;
  while (str[i])
    {
      if (str[i] != ':')
	{
	  tab[m] = my_patht(str + i, command);
	  m = m + 1;
	  while (str[i] && str[i] != ':')
	    ++i;
	}
      if (str[i])
	i = i + 1;
    }
  tab[m] = 0;
  return (tab);
}
