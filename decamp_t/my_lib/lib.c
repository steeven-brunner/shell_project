/*
** lib.c for dash in /home/decamp_t/rendu/42sh/decamp_t/my_lib
** 
** Made by decamp_t
** Login   <decamp_t@epitech.net>
** 
** Started on  Tue May 13 15:34:17 2014 decamp_t
** Last update Tue May 13 15:38:52 2014 decamp_t
*/

#include <unistd.h>

int	my_strlen(char *str)
{
  int	i;

  i = 0;
  while (str[i])
    i++;
  return (i);
}

void	my_putchar(char t)
{
  write(1, &t, 1);
}

void	my_putstr(char *str)
{
  int	i;

  i = 0;
  while (str[i])
    {
      my_putchar(str[i]);
      i++;
    }
  return (0);
}
