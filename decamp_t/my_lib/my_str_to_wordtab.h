/*
** my_str_to_wordtab.h for wolf in /home/decamp_t/rendu/PSU_2013_minishell1
** 
** Made by decamp_t
** Login   <decamp_t@epitech.net>
** 
** Started on  Fri Jan  3 10:47:19 2014 decamp_t
** Last update Mon Jan  6 18:56:44 2014 decamp_t
*/

#ifndef MY_STR_TO_WORDTAB_H_
# define MY_STR_TO_WORDTAB_H_

int	my_strlen(char *str);
int	calc_nbr_word(char *str);
int	calc_nbr_car(char *str, int *i);
char	*my_strt(char *src);
char	*my_patht(char *src, char *command);
char	**my_str_to_wordtab(char *str);
int	calc_nbr_word_p(char *str);
int	calc_nbr_car_p(char *str, int *i);
char	**my_path_to_wordtab(char *str, char *command);

#endif /* !MY_STR_TO_WORDTAB_H_ */
