/*
** my_strncmp.c for my_strncmp in /home/decamp_t/rendu/Piscine-C-Jour_06/ex_06
** 
** Made by decamp
** Login   <decamp_t@epitech.net>
** 
** Started on  Mon Oct  7 15:35:30 2013 decamp
** Last update Mon Jan  6 15:00:18 2014 decamp_t
*/

int	my_strcmp(char *s1, char *s2)
{
  int	i;

  i = 0;
  while (s2[i] != 0)
    {
      if (s1[i] != s2[i])
	return (1);
      if (s1[i] == '\0')
	return (1);
      if (s2[i] == '\0')
	return (1);
      i = i + 1;
    }
  return (0);
}
