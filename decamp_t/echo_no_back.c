/*
** echo.c for dash in /home/decamp_t/rendu/42sh/decamp_t
** 
** Made by decamp_t
** Login   <decamp_t@epitech.net>
** 
** Started on  Tue May 13 15:33:53 2014 decamp_t
** Last update Sat May 24 22:47:55 2014 decamp_t
*/

#include <stdio.h>

#include "mysh.h"

int	my_echo_ret_no_back(char *str, t_ret prev)
{
  printf("%d", prev->ret);
  return (0);
}

int	my_echo_env_no_back(char *str, t_env *myenv)
{
  if (myenv != NULL)
    {
      while (myenv->next != NULL)
	{
	  if (strcmp(myenv->name, str) == 0)
	    {
	      printf("%s", myenv->data);
	      return (0);
	    }
	  myenv = ->next;
	}
    }
  return (1);
}

int	no_back(char **str, t_mysh *mysh)
{
  int	i;

  i = 2;
  if (str[i] == NULL)
    return (0);
  else
    while (str[i] != NULL)
      {
	if (str[i][0] == '$' && str[i][1] != '?')
	  mysh->prev->ret = my_echo_env_no_back((str[i] + 1), mysh->myenv);
	else if (str[i][0] == '$' && str[i][1] == '?')
	  mysh->prev->ret = my_echo_ret_no_back(str[i], mysh->prev);
	else
	  {
	    printf("%s", str[i]);
	    mysh->prev->ret = 0;
	  }
	i++;
      }
  return (mysh->prev->ret);
}
