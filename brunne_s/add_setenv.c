/*
** add_setenv.c for dash in /home/decamp_t/rendu/42sh/decamp_t/mise_en_norme
** 
** Made by decamp_t
** Login   <decamp_t@epitech.net>
** 
** Started on  Tue May 13 16:14:49 2014 decamp_t
** Last update Tue May 13 16:35:15 2014 decamp_t
*/

void		add_setenv_next(t_list **env_list, char *name_e,
				char *content_e, t_list *elem)
{
  int		i;

  i = 0;
  while (content_e[i] != '\0')
    {
      elem->content[i] = content_e[i];
      i++;
    }
  elem->content[i] = '\0';
  elem->next = NULL;
  *env_list = elem;
}

void		add_setenv(t_list **env_list, char *name_e, char *content_e)
{
  t_list	*elem;
  t_list	*end;
  int		i;

  i = 0;
  elem = malloc(sizeof(*elem));
  elem->next = NULL;
  elem->name = malloc((my_strlen(name_e) + 1) * sizeof(*elem->name));
  elem->content = malloc((my_strlen(content_e) + 1) * sizeof(*elem->content));
  end = end_of_list(*env_list);
  end->next = elem;
  while (name_e[i] != '\0')
    {
      elem->name[i] = name_e[i];
      i++;
    }
  elem->name[i] = '\0';
  add_setenv_next(env_list, name_e, content_e, elem);
}
