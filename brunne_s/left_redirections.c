/*
** simple_redirection.c for simple_redirection in /home/brunne_s/rendu/Communaute_du_C/42sh/redirection
** 
** Made by brunner steeven
** Login   <brunne_s@epitech.net>
** 
** Started on  Wed Apr  9 14:31:23 2014 brunner steeven
** Last update Wed May  7 17:57:06 2014 brunner steeven
*/

#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>
#include <string.h>

void    simple_left_redirections(/*Liste chainée */)
{
  int   fd_dest;
  int	fd_src;
  
  if ((fd_dest = open(/* FILE */"test", O_WRONLY | O_CREAT, 0644)) == -1)
    {
      printf("[Error] : open\n");
      return ;
    }
  dup2(fd_dest, 0);
  /*Exec*/printf("YOLO\n");
  close(fd_dest);
  close(fd_src);
  /*builtin_exit()*/exit(42);
}

char	*delete_end(char *str)
{
  char	*str2;
  int	i;

  i = 0;
  str2 = malloc(strlen(str) * sizeof(*str2));
  while (str[i] != '\0' && str[i] != '\n')
    {
      str2[i] = str[i];
      i++;
    }
  return (str2);
}

char	*my_realloc(char *str, char *buffer)
{
  char	*save;
  int	i;
  int	j;

  i = 0;
  j = 0;
  if ((save = malloc(sizeof(save) * (strlen(str) + 10))) == NULL)
    return (NULL);
  while (str[i] != '\0')
    {
      save[i] = str[i];
      i++;
    }
  while (buffer[j] != '\0')
    {
      save[i] = buffer[j];
      j++;
      i++;
    }
  save[i] = '\0';
  return (save);
}

void	double_left_redirection(/* Liste chainée */)
{
  int	size;
  char	*buff;
  char	*full_buff;
  char	*short_buff;
  int	fd[2];
  
  size = 100;
  full_buff = "\0";
  while (42)
    {
      buff = malloc((size + 100) * sizeof(*buff));
      size = read(0, buff, 100);
      short_buff = delete_end(buff);
      full_buff = my_realloc(full_buff, buff);
      if (strcmp(short_buff,  /*FILE*/ ) == 0)
	{
	  pipe(fd);
	  dup2(fd[1], 1);
	  dup2(fd[0], 0);
	  //EXEC
	  //close(fd);
	  //builtin_exit();
	  return ;
	}
      free(short_buff);
      free(buff);
    }
}

int	main()
{
  double_left_redirection();
}
