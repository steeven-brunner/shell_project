/*
** all_env.c for all_env in /home/brunne_s/rendu/Communaute_du_C/minishell1/list_path
** 
** Made by brunner steeven
** Login   <brunne_s@epitech.net>
** 
** Started on  Sun Feb 23 17:36:29 2014 brunner steeven
** Last update Tue May 13 17:19:20 2014 decamp_t
*/

#include <stdlib.h>
#include <stdio.h>
#include "my_list.h"

void		put_env_in_list_next(t_list **env_list, char *line_env,
				     t_list *elem, int mv_content)
{
  while (line_env[i] != '\0')
    {
      elem->content[mv_content] = line_env[i];
      i++;
      mv_content++;
    }
  elem->content[mv_content] = '\0';
  if (*env_list == NULL)
    elem->next = NULL;
  else
    elem->next = *env_list;
  *env_list = elem;
}

void		put_env_in_list(t_list **env_list, char *line_env,
                                int size_name, int size_content)
{
  t_list	*elem;
  int		i;
  int		mv_content;

  i = 0;
  mv_content = 0;
  elem = malloc(sizeof(*elem));
  elem->next = NULL;
  elem->name = malloc(size_name * sizeof(*elem->name));
  elem->content = malloc(size_content * sizeof(*elem->content));
  while (line_env[i] != '=')
    {
      elem->name[i] = line_env[i];
      i++;
    }
  elem->name[i] = '\0';
  i++;
  put_env_in_list_next(env_list, line_env, elem, mv_content);
}

t_list	*end_of_list(t_list *elem)
{
  while (elem->next != NULL)
    elem = elem->next;
  return (elem);
}

void	builtin_env(t_list *env, char *buff)
{
  if (strcmp("env\n", buff) == 0)
    {
      while (env != NULL)
        {
          printf("%s=%s\n", env->name, env->content);
          env = env->next;
        }
    }
}
