/*
** builtins.c for builtins.c in /home/brunne_s
** 
** Made by brunner steeven
** Login   <brunne_s@epitech.net>
** 
** Started on  Fri Feb 28 15:21:19 2014 brunner steeven
** Last update Tue May 13 15:59:56 2014 decamp_t
*/

#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include "my_list.h"

void	b_setenv_next(char **buff, t_list *env)
{
  env->next->name = malloc (my_strlen(buff[1]) * sizeof(*env->name));
  env->next->name = buff[1];
  if (buff[2] != NULL)
    {
      env->next->content = malloc(my_strlen(buff[2]) * sizeof(*env->content));
      env->next->content = buff[2];
    }
}

void	b_setenv(char **buff, t_list *env)
{
  if (my_strcmp("setenv", buff[0]) == 0)
    {
      if (buff[1] == NULL)
        {
          my_putstr("[Error] : choose an environment variable\n");
          return ;
        }
      if (buff[2] == NULL)
        {
          buff[2] = malloc(2 * sizeof(char));
          buff[2][0] = '\0';
        }
      while (env != NULL && env->next != NULL &&
             my_strcmp(env->next->name, buff[1]) != 0)
        env = env->next;
      if (env->next == NULL)
        add_setenv(&env, buff[1], buff[2]);
      else
	b_setenv_next(buff, env);
    }
}

void	b_unsetenv(char **buff, t_list *env)
{
  if (my_strcmp("unsetenv", buff[0]) == 0)
    {
      if (buff[1] == NULL)
	{
	  my_putstr("[Error] : choose an environment variable\n");
	  return ;
	}
      while (env != NULL && env->next != NULL &&
	     my_strcmp(env->next->name, buff[1]) != 0)
	env = env->next;
      if (env != NULL && env->next != NULL &&
	  my_strcmp(env->next->name, buff[1]) == 0)
	env->next = env->next->next;
    }
}
