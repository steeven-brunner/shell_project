/*
** strlen_env.c for dash in /home/decamp_t/rendu/42sh/decamp_t/mise_en_norme
** 
** Made by decamp_t
** Login   <decamp_t@epitech.net>
** 
** Started on  Tue May 13 16:10:12 2014 decamp_t
** Last update Tue May 13 16:10:31 2014 decamp_t
*/

int	strlen_env_name(char *line_env)
{
  int	i;
  int	size_name;

  i = 0;
  size_name = 0;
  while (line_env[i] != '=')
    {
      i++;
      size_name++;
    }
  return (size_name);
}

int	strlen_env_content(char *line_env)
{
  int	i;
  int	size_content;

  i = 0;
  size_content = 0;
  while (line_env[i] != '=')
    i++;
  i++;
  while (line_env[i] != '\0')
    {
      i++;
      size_content++;
    }
  return (size_content);
}
