/*
** builtin_cd.c for builtin_cd.c in /home/brunne_s/rendu/Communaute_du_C/Backdoor_42
** 
** Made by brunner steeven
** Login   <brunne_s@epitech.net>
** 
** Started on  Fri May  9 17:50:52 2014 brunner steeven
** Last update Mon May 19 19:10:20 2014 brunner steeven
*/

void	b_cd(char **buff, t_list *env)
{
  char	*full_track;
  int	size_malloc;

  while (env != NULL && env->next != NULL &&
	 my_strcmp(env->name, "PWD") != 0)
    env = env->next;
  if (env == NULL)
    return ;
  size_malloc = my_strlen(env->content) + my_strlen(buff[1]) + 1;
  full_track = malloc(size_malloc * sizeof(char));
  my_strcat(full_track, env->content);
  my_strcat(full_track, "/");
  my_strcat(full_track, buff[1]);
  if (access(full_track, X_OK) == -1)
    {
      my_putstr("[Error] : permission denied\n");
      return ;
    }
  else
    {
      chdir(full_track);
      env->content = malloc(size_malloc * sizeof(*env->content));
      my_strcat(env->content, full_track);
    }
}

void	b_cd_less(char **buff, t_list *env)
{
  char	*full_track;
  int	size_malloc;

  while (env != NULL && env->next != NULL &&
	 my_strcmp(env->name, "OLD_PWD") != 0)
    env = env->next;
  if (env == NULL)
    return ;
  size_malloc = my_strlen(env->content) + my_strlen(buff[1]) + 1;
  full_track = malloc(size_malloc * sizeof(char));
  my_strcat(full_track, env->content);
  my_strcat(full_track, "/");
  my_strcat(full_track, buff[1]);
  if (access(full_track, X_OK) == -1)
    {
      my_putstr("[Error] : permission denied\n");
      return ;
    }
  else
    {
      chdir(full_track);
      env->content = malloc(size_malloc * sizeof(*env->content));
      my_strcat(env->content, full_track);
    }
}

void	b_cd_tild(char **buff, t_list *env)
{
  char	*full_track;
  int	size_malloc;

  while (env != NULL && env->next != NULL &&
         my_strcmp(env->name, "HOME") != 0)
    env = env->next;
  if (env == NULL)
    return ;
  size_malloc = my_strlen(env->content) + my_strlen(buff[1]) + 1;
  full_track = malloc(size_malloc * sizeof(char));
  my_strcat(full_track, env->content);
  my_strcat(full_track, "/");
  my_strcat(full_track, buff[1]);
  if (access(full_track, X_OK) == -1)
    {
      my_putstr("[Error] : permission denied\n");
      return ;
    }
  else
    {
      chdir(full_track);
      env->content = malloc(size_malloc * sizeof(*env->content));
      my_strcat(env->content, full_track);
    }
}
