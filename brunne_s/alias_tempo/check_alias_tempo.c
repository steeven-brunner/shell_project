/*
** check_alias_tempo.c for check_alias_tempo.c in /home/brunne_s/rendu/42sh/alias_tempo
** 
** Made by brunner steeven
** Login   <brunne_s@epitech.net>
** 
** Started on  Wed May 21 13:58:07 2014 brunner steeven
** Last update Wed May 21 13:58:30 2014 brunner steeven
*/

char            *check_alias_tempo(char *command, t_tempo *list_tempo)
{
  t_tempo       *tmp;

  tmp = list_tempo;
  while (tmp != NULL)
    {
      if (strcmp(command, tmp->name) == 0)
        return (tmp->content);
      tmp = tmp->next;
    }
  return (NULL);
}
