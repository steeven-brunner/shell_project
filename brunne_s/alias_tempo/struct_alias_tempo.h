/*
** struct_alias.h for struct_alias.h in /home/brunne_s/rendu/42sh
** 
** Made by brunner steeven
** Login   <brunne_s@epitech.net>
** 
** Started on  Tue May 13 16:34:41 2014 brunner steeven
** Last update Wed May 21 14:46:08 2014 brunner steeven
*/

#ifndef STRUCT_ALIAS_TEMPO_H_
# define STRUCT_ALIAS_TEMPO_H_

typedef struct	s_alias_tempo
{
  int		fd;
  int		i;
  int		mv_i;
  int		mv_j;
  int		mv_tab;
  char		*str;
  char		*str_path;
  char		**tab;
}		t_alias_tempo;

typedef struct		s_tempo
{
  char			*name;
  char			*content;
  struct s_tempo	*next;
}			t_tempo;

#endif /* !STRUCT_ALIAS_H_ */
