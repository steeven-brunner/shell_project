/*
** alias.c for alias.c in /home/brunne_s/rendu/42sh
** 
** Made by brunner steeven
** Login   <brunne_s@epitech.net>
** 
** Started on  Tue May 13 15:56:21 2014 brunner steeven
** Last update Wed May 21 14:44:20 2014 brunner steeven
*/

#include <stdlib.h>
#include <fcntl.h>
#include <string.h>
#include <stdio.h>
#include <unistd.h>
#include "struct_alias_tempo.h"

void		add_list_alias_t(t_tempo **list_tempo, char *name_a,
				 char *content_a)
{
  t_tempo	*elem;
  t_tempo	*tmp;
  int		i;

  i = 0;
  elem = malloc(sizeof(*elem));
  elem->name = strdup(name_a);
  elem->content = strdup(content_a);
  elem->next = NULL;
  if (*list_tempo == NULL)
    *list_tempo = elem;
  else
    {
      tmp = *list_tempo;
      while (tmp->next != NULL)
	tmp = tmp->next;
      tmp->next = elem;
    }
}
