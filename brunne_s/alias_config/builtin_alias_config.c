/*
** alias.c for alias.c in /home/brunne_s/rendu/42sh
** 
** Made by brunner steeven
** Login   <brunne_s@epitech.net>
** 
** Started on  Tue May 13 15:56:21 2014 brunner steeven
** Last update Tue May 20 17:17:50 2014 brunner steeven
*/

#include <stdlib.h>
#include <fcntl.h>
#include <string.h>
#include <stdio.h>
#include <unistd.h>
#include "struct_alias.h"

void	init_struct_alias(t_alias **alias)
{
  *alias = malloc(sizeof(t_alias));
  (*alias)->i = 0;
  (*alias)->mv_i = 0;
  (*alias)->mv_j = 0;
  (*alias)->mv_tab = 0;
}

void	malloc_tab_alias(t_alias *alias)
{
 alias->tab = malloc(60 * sizeof(char));
 alias->str = malloc(3000 * sizeof(char));
 alias->str_path = malloc(150 * sizeof(char));
 while (alias->mv_tab < 50)
   {
     alias->tab[alias->mv_tab] = malloc(50 * sizeof(char));
     alias->mv_tab = alias->mv_tab + 1;
   }
}

void	alias_found(t_alias *alias)
{ 
  alias->i = alias->i + 6;
  while (alias->str[alias->i] != '=')
    {
      alias->tab[alias->mv_j][alias->mv_i] = alias->str[alias->i];
      alias->i++;
      alias->mv_i++;
    }
  alias->tab[alias->mv_j][alias->mv_i] = '\0';
  alias->i = alias->i + 2;
  alias->mv_i = 0;
  alias->mv_j = alias->mv_j + 1;
  while (alias->str[alias->i] != '\'')
    {
      alias->tab[alias->mv_j][alias->mv_i] = alias->str[alias->i];
      alias->i = alias->i + 1;
      alias->mv_i = alias->mv_i + 1;
    }
  alias->tab[alias->mv_j][alias->mv_i] = '\0';
  alias->mv_j = alias->mv_j + 1;
  alias->mv_i = 0;
}

char		*search_path(t_alias *alias, t_list *env)
{
  char		*str;
  t_list	tmp;

  tmp = env;
  while (tmp->name != NULL)
    {
      if (strcmp(tmp->name, "PWD") != 0)
	{
	  str = malloc((my_strlen(tmp->content) + 1) * sizeof(char));
	  strcpy(str, tmp->content);
	  return (str);
	}
      tmp = tmp->next;
    }
  str[0] = '\0';
  printf("[Error] : PWD not found in this env\n");
  exit (0);
}

char	**builtin_alias_config(char *path, t_alias *alias, t_list *env)
{
  init_struct_alias(&alias);
  malloc_tab_alias(alias);
  alias->str_search = search_path(alias, env);
  alias->fd = open(str_search, O_RDONLY);
  if (alias->fd == -1)
    {
      printf("[Error] : open\n");
      return (NULL);
    }
  alias->mv_tab = 0;
  read(alias->fd, alias->str, 3000);
  while (alias->str[alias->i] != '\0')
    {
      if (alias->str[alias->i] == 'a' && alias->str[alias->i + 1] == 'l'
	  && alias->str[alias->i + 2] == 'i'
	  && alias->str[alias->i + 3] == 'a' && alias->str[alias->i + 4] == 's')
	alias_found(alias);
      alias->i++;
    }
  alias->tab[alias->mv_j] = NULL;
  close(alias->fd);
  return (alias->tab);
}
