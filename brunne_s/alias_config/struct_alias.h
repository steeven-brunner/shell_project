/*
** struct_alias.h for struct_alias.h in /home/brunne_s/rendu/42sh
** 
** Made by brunner steeven
** Login   <brunne_s@epitech.net>
** 
** Started on  Tue May 13 16:34:41 2014 brunner steeven
** Last update Tue May 20 17:19:17 2014 brunner steeven
*/

#ifndef STRUCT_ALIAS_H_
# define STRUCT_ALIAS_H_

typedef struct	s_alias
{
  int		fd;
  int		i;
  int		mv_i;
  int		mv_j;
  int		mv_tab;
  char		*str;
  char		*str_path;
  char		**tab;
}		t_alias;

char	*check_alias(char *command, char **tab)

#endif /* !STRUCT_ALIAS_H_ */
