/*
** check_alias.c for check_alias.c in /home/brunne_s/rendu/42sh/alias_config
** 
** Made by brunner steeven
** Login   <brunne_s@epitech.net>
** 
** Started on  Tue May 20 17:13:38 2014 brunner steeven
** Last update Wed May 21 13:56:10 2014 brunner steeven
*/

#include "struct_alias.h"

char	*check_alias_config(char *command, char **tab)
{
  int	j;

  j = 0;
  while (tab[j] != NULL)
    {
      if (j%2 == 0 && (strcmp(command, tab[j]) == 0))
        return (tab[j + 1]);
      j++;
    }
  return (NULL);
}
