/*
** simple_redirection.c for simple_redirection in /home/brunne_s/rendu/Communaute_du_C/42sh/redirection
** 
** Made by brunner steeven
** Login   <brunne_s@epitech.net>
** 
** Started on  Wed Apr  9 14:31:23 2014 brunner steeven
** Last update Wed May 21 15:23:26 2014 brunner steeven
*/

#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>

void    simple_right_redirection(/*Liste chainée, */)
{
  int   val_open_old;

  val_open_old = open(/* FILE */"LOL", O_WRONLY | O_CREAT, 0644);
  if (val_open_old == -1)
    {
      printf("[Error] : open\n");
      return ;                                   // FONCTIONNEL
    }
  dup2(val_open_old, 1);
  //   Exec  //
  printf("Test\n");// -------
  close(val_open_old);
  //builtin_exit();
}

void	double_right_redirection(/* Liste chainée, */)
{
  int   val_open_old;

  val_open_old = open(/* FILE */"LOL", O_WRONLY | O_CREAT | O_APPEND, 0644);
  if (val_open_old == -1)
    {
      printf("[Error] : open\n");
      return ;
    }
  dup2(val_open_old, 1);                         // FONCTIONNEL
  //   Exec  //
  printf("Test\n");// ------
  close(val_open_old);
  //builtin_exit();
}
