/*
** my_list.h for my_list.h in /home/brunne_s/rendu/Communaute_du_C/minishell1/list_path
** 
** Made by brunner steeven
** Login   <brunne_s@epitech.net>
** 
** Started on  Tue Feb 18 10:01:59 2014 brunner steeven
** Last update Tue Feb 18 10:06:04 2014 brunner steeven
*/

#ifndef MY_LIST_H_
# define MY_LIST_H_

typedef struct s_list
{
  char		*name;
  char		*content;
  struct s_list	*next;
}		t_list;

#endif /* !MY_LIST_ */
