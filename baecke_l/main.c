/*
** main.c for main.c in /home/baecke_l/rendu/42sh/baecke_l/dup2
** 
** Made by Baecker Leo
** Login   <baecke_l@epitech.net>
** 
** Started on  Fri May 23 21:11:14 2014 Baecker Leo
** Last update Sun May 25 06:39:53 2014 Baecker Leo
*/

#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>

int	main(int argc, char **argv)
{
  int	pipefd[2];
  int	pipefd2[2];
  int	pipefd3[2];
  int	pid;
  int	pid2;
  int	pid3;
  int	error;

  pipe(pipefd);
  pid = fork();
  if (pid == 0)
    {
      close(pipefd[0]);
      dup2(pipefd[1], 1);
      execlp("ls", "ls", 0);
      close(pipefd[1]);
    }
  else
    {
      pipe(pipefd2);
      pid2 = fork();
      if (pid2)
	{
	  close(pipefd[1]);
	  close(pipefd2[0]);
	  dup2(pipefd[0], 0);
	  dup2(pipefd2[1], 1);
	  execlp("cat", "cat", "toto", 0);
	  close(pipefd[0]);
	  close(pipefd2[1]);
	}
      else
	{
	  close(pipefd2[1]);
	  dup2(pipefd2[0], 0);
	  execlp("grep", "grep", "bonjour", 0);
	  close(pipefd2[0]);
	}
    }
  exit(EXIT_SUCCESS);
}
